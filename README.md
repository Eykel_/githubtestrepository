Projeto que implementa alguns endpoints da api de consulta do GitHub (https://api.github.com/)

O propósito do projeto, na época, era criar uma estrutura que consumisse da api do github e tivesse um sistema de favoritar.
Além das requests para coletar os dados, foi feito a inserção de alguns dados no banco de dados local, para que fosse criado um sistema de usuários favoritos.

No mais, projeto padrão e simples utilizando mvvm, retrofit para request e room salvar dados locais.
Devido a complexidade simples, não houve necessidade de fazer sistema de interface entre as camadas, useCase ou até camada de networking com interface.

Para você que esta vendo isso, esse projeto é de 2021, de la pra cá, algumas coisas melhoraram, recomendaria limpar mais a view, adicionando algumas extensions, 
como extensions pros Observers e outras mais, pra incentivar o reuso e limpar mais a view, tem bastante coisa ali, possívelmente se tivesse um lint ou detekt nesse projeto, a view não passaria.

Recomendaria também encapsular os try catch das requests, parece bobo, mas uma estrutura tipo 
doRequest()
	.onSucces {}
	.onFailure {}

Confia, isso ajuda muito, evita repetições e deixa o trêm mais bonito.

Injeção de dependencias, recomendaria o Koin, hoje em dia, virou indispensável, caso você esteja vendo isso depois de 2023, saiba que em 2023 era assim que se pensava ainda.
Você poderia usar o dagger, mas confia, em projeto grande, vai dar problema, quando o pessoal começar a pensar em tempo de build. E o Koin também, ao meu ver, é mais fácil, confia.

E por ultimo, para testes, mockk pra mocar as dependencias necessárias. 
Recomendável fazer da sua viewModel, seus mappers (caso você tenha), suas extensions e coisas do tipo.
Caso você esteja pensando em fazer em composer, lembrar de fazer testes de view de composer. Não se preocupe, são mais simples dos que o de viewModel e talz.

E um adendo, caso você esteja vendo isso depois de 2023, saiba que em 2023 composer ja ta em alta, então considere abandonar o xml :)