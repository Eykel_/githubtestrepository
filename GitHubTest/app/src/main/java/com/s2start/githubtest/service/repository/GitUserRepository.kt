package com.s2start.githubtest.service.repository

import android.content.Context
import com.s2start.githubtest.service.model.GitUser
import com.s2start.githubtest.service.repository.local.GitUserDataBase
import com.s2start.githubtest.service.repository.remote.RetrofitBuilder

class GitUserRepository (context: Context){

    private val mGitHubApi = RetrofitBuilder().gitHubApi
    private val mDataBase =  GitUserDataBase.getInstance(context).userDAO()

    suspend fun getGitUserList(query: String) = mGitHubApi.getUserList(query)
    suspend fun getUserDetail(login: String) = mGitHubApi.getUserDetail(login)

    fun save(user: GitUser) = mDataBase.insert(user) > 0
    fun delete(user: GitUser) =  mDataBase.delete(user) > 0
    fun getAll() =  mDataBase.getAll()
    fun getFavoriteList() = mDataBase.getFavoriteList()
}