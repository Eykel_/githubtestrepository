package com.s2start.githubtest.view

import android.view.View

interface GitUserListener {
    fun onClick(id: Int)
    fun onFavoriteClick(id: Int)
}